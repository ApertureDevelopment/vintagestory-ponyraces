# Vintage Story Pony Races

the ride never ends

horse races for horse mod

beta, expect breaking changes

also a test mod for kemono race system


# Races Overview

| Name        | Req               | Traits                                |
|-------------|-------------------|---------------------------------------|
| Earth Pony  | - No wings        | +10% mining speed                     |
|             | - No horn         | +20% crop drop                        |
|             |                   | +20% crop drop                        |
|             |                   | +Buck trees                           |
|             |                   |                                       |
| Pegasus     | - Pegasus wings   | +20s fly time (~60s recovery)         |
|             | - No horn         | +30% fall damage resistance           |
|             |                   | +10% melee damage                     |
|             |                   | -60% crop drop                        |
|             |                   |                                       |
| Unicorn     | - Unicorn horn    | +4.5 block range                      |
|             | - No wings        | +Horn light                           |
|             |                   | +Levitate entities                    |
|             |                   | -40% crop drop                        |
|             |                   |                                       |
| Kirin       | - Kirin pattern   | +10% melee damage                     |
|             | - Kirin horn      | +1.5 block range                      |
|             |                   | +Horn light                           |
|             |                   | +Fire damage resistance               |
|             |                   | +Instant light kiln, firepit, etc.    |
|             |                   | -Stability loss when <50% HP          |
|             |                   | -Nirik when stability <50%            |
|             |                   |                                       |
| Bat         | - Bat wings       | +10% mining speed                     |
|             |                   | +20s fly time (~75s recovery) in dark |
|             |                   | +30% fall damage resistance           |
|             |                   | +Night vision                         |
|             |                   | -40% crop drop                        |
|             |                   |                                       |
| Zebra       | - Zebra stripes   | +Alchemy (requires Alchemy mod)       |
|             | - No wings        | +5% walk speed                        |
|             |                   | +10% mining speed                     |
|             |                   | +20% crop drop                        |
|             |                   | +Buck trees                           |
|             |                   |                                       |
| Alicorn     | - Unicorn horn    | +4.5 block range                      |
|             | - Pegasus wings   | +20s fly time (~60s recovery)         |
|             | - Jannie only     | +Horn light                           |
|             |                   | +10% mining speed                     |
|             |                   | +20% crop drop                        |
|             |                   | +Levitate entities                    |
|             |                   | +Buck trees                           |


# Race Mechanics Detailed

## Earth Pony
- Can buck trees (default hotkey K)
- Fruit trees and berry bushes will drop all their fruit
- Regular trees leaves will drop sticks and seeds
- Berry bushes will drop their fruit

## Pegasus
- Can fly (default controls are space + space)
- Reduced fall damage
- (TODO) Carry other horses?

## Unicorn
- Horn light: bright light from horn (default hotkey L)
- Telekinesis: longer block range
- Levitate other entities (default hotkey V)

## Kirin
- Horn light: bright light from horn (default hotkey L)
- Telekinesis: longer block range
- Lose stability when HP <50%
- Become Nirik mode when stability <50%, lights entities on fire
- Summon fire to act like a torch, at cost of stability (default hotkey M)

## Bat
- Can fly (default controls are space + space), but...
- Increased fly stamina loss when in bright light
- Reduced fall damage
- Night vision (default hotkey N)

## Zebra (least finished)
- Can also buck like earth pony
- Alchemy: can make potion bases (requires Alchemy mod)
TODO: should involve cooking, healing

## Alicorn
- nobility


# Dev stuff

This is setup for development from VS Code.

1. Download C#/dotnet/VS code plugins: <https://code.visualstudio.com/docs/languages/dotnet>

2. Launch run task in VS code (clr or mono)

3. Build actual release .zip using:  
`dotnet build -c release`

