using System;
using System.Diagnostics;
using System.IO;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace ponyraces
{
    /// <summary>
    /// Contains kirin fire related functions.
    /// </summary>
    public partial class PonyRaceMod
    {
        public AdvancedParticleProperties ParticlesKirinFireCyan = new AdvancedParticleProperties
        {
            ParticleModel = EnumParticleModel.Quad,
            Quantity = new NatFloat(80f, 0f, EnumDistribution.UNIFORM),
            Size = NatFloat.createUniform(1.0f, 0.8f),
            HsvaColor = null,
            Color = ColorUtil.ToRgba(150, 70, 235, 255),
            VertexFlags = 175,
            LifeLength = NatFloat.createUniform(0.2f, 0.05f),
            GravityEffect = NatFloat.Zero,
            Velocity = new NatFloat[]
            {
                NatFloat.createUniform(0f, 1.2f),
                NatFloat.createUniform(1.5f, 0.5f),
                NatFloat.createUniform(0f, 1.2f)
            },
            PosOffset = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.5f),
                NatFloat.createUniform(0f, 0.5f),
                NatFloat.createUniform(0f, 0.5f)
            }
        };

        public AdvancedParticleProperties ParticlesKirinFirePink = new AdvancedParticleProperties
        {
            ParticleModel = EnumParticleModel.Quad,
            Quantity = new NatFloat(80f, 0f, EnumDistribution.UNIFORM),
            Size = NatFloat.createUniform(0.8f, 0.3f),
            HsvaColor = null,
            Color = ColorUtil.ToRgba(125, 255, 35, 70),
            VertexFlags = 175,
            LifeLength = NatFloat.createUniform(0.25f, 0.05f),
            GravityEffect = NatFloat.Zero,
            Velocity = new NatFloat[]
            {
                NatFloat.createUniform(0f, 1.25f),
                NatFloat.createUniform(1.75f, 0.5f),
                NatFloat.createUniform(0f, 1.25f)
            },
            PosOffset = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.5f),
                NatFloat.createUniform(0f, 0.5f),
                NatFloat.createUniform(0f, 0.5f)
            }
        };

        public float KirinFireRadius = 0.75f;
        public float KirinFireHeight = 1.0f;
        public AssetLocation KirinFireSound = new AssetLocation("game:sounds/effect/smallexplosion.ogg");

        /// <summary>
        /// Make pony buck block behind it. Cancels if pony does not have 
        /// trait requirements or if block is not buckable.
        /// </summary>
        /// <param name="entity"></param>
        public void TryKirinFire(Entity entity)
        {
            if (api.Side != EnumAppSide.Server) return;

            // use up stamina
            var staminaBehavior = entity.GetBehavior<EntityBehaviorPonyStamina>();
            if (staminaBehavior != null)
            {
                staminaBehavior.CurrentStamina = 0f;
            }

            // play animation
            // WHY THE FUCK DOESNT THIS WORK?
            // entity.AnimManager.StartAnimation(new AnimationMetaData() { Animation = "summonfire", Code = "summonfire", AnimationSpeed = 1f }.Init());
            // have to manually send packet to client and force animation...
            serverToClientChannel.BroadcastPacket(new PacketServerStartAnimation
            {
                EntityId = entity.EntityId,
                Code = "summonfire"
            });

            // project forward by 1 block to get fire location
            Vec3d forwardDirection = MathUtils.Vec3dFromYaw(entity.SidedPos.Yaw);
            Vec3d firePos = entity.SidedPos.XYZ.AddCopy(forwardDirection);

            // delay handling to sync with animation more to look cooler
            api.Event.RegisterCallback((dt) => FinishKirinFire(entity, firePos), 100);
        }
        
        public void FinishKirinFire(Entity entity, Vec3d firePos)
        {
            BlockPos position = new BlockPos((int)firePos.X, (int)firePos.Y, (int)firePos.Z, entity.Pos.Dimension);
            Block block = entity.World.BlockAccessor.GetBlock(position);

            // consume temporal stability
            if (Config.KirinFireStabilityCost > 0)
            {
                double stability = entity.WatchedAttributes.GetDouble("temporalStability", 1);
                double newStability = GameMath.Clamp(stability - Config.KirinFireStabilityCost, 0, 1);
                entity.WatchedAttributes.SetDouble("temporalStability", newStability);
            }

            // consume hunger
            if (Config.KirinFireHungerCost > 0)
            {
                entity.GetBehavior<EntityBehaviorHunger>()?.ConsumeSaturation(Config.KirinFireHungerCost);
            }

            // play sound and summon particles
            api.World.PlaySoundAt(KirinFireSound, firePos.X, firePos.Y, firePos.Z, null, false);

            // crashes when run on server...
            // // cyan
            // ParticlesKirinFireCyan.basePos = firePos;
            // entity.World.SpawnParticles(ParticlesKirinFireCyan);

            // // pink
            // firePos = firePos.AddCopy(0.0, 0.3, 0.0);
            // ParticlesKirinFirePink.basePos = firePos;
            // entity.World.SpawnParticles(ParticlesKirinFirePink);
            serverToClientChannel.BroadcastPacket(new PacketServerKirinFireParticles {
                X = (float) firePos.X,
                Y = (float) firePos.Y,
                Z = (float) firePos.Z
            });
            
            // set nearby entities on fire
            Entity[] entities = entity.World.GetEntitiesAround(
                firePos,
                KirinFireRadius,
                KirinFireHeight,
                (e) => e is EntityAgent && e.Alive && e.EntityId != entity.EntityId
            );

            foreach (var nearbyEntity in entities)
            {
                nearbyEntity.WatchedAttributes.SetBool("onFire", true);
            }

            // block specific fire interactions
            if (block is BlockFirepit) // light fire pit
            {
                api.Event.RegisterCallback((dt) => TryIgniteFirepit(position), 100);
            }
            else if (block is BlockPitkiln) // light pit kiln
            {
                api.Event.RegisterCallback((dt) => TryIgnitePitkiln(position), 100);
            }
            else if (block is BlockForge) // light forge
            {
                api.Event.RegisterCallback((dt) => TryIgniteForge(position), 100);
            }
            else if (block is BlockBloomery) // light bloomery
            {
                api.Event.RegisterCallback((dt) => TryIgniteBloomery(position), 100);
            }
            else if (block is BlockAnvil) // heat part on anvil
            {
                api.Event.RegisterCallback((dt) => TryHeatAnvil(position), 100);
            }
            else
            {
                // check if block underneath is a pitkiln
                BlockPos belowPos = position.DownCopy();
                Block belowBlock = entity.World.BlockAccessor.GetBlock(belowPos);
                if (belowBlock is BlockPitkiln)
                {
                    api.Event.RegisterCallback((dt) => TryIgnitePitkiln(belowPos), 100);
                }
            }
        }

        
        public void OnClientReceiveKirinFireParticles(PacketServerKirinFireParticles p)
        {
            if (api.Side != EnumAppSide.Client) return;

            Vec3d pos = new Vec3d(p.X, p.Y + 0.25, p.Z);

            // cyan
            ParticlesKirinFireCyan.basePos = pos;
            api.World.SpawnParticles(ParticlesKirinFireCyan);

            // pink
            pos = pos.AddCopy(0.0, 0.5, 0.0);
            ParticlesKirinFirePink.basePos = pos;
            api.World.SpawnParticles(ParticlesKirinFirePink);
        }


        // reference:
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/Block/BlockFirepit.cs#L164
        public void TryIgniteFirepit(BlockPos pos)
        {
            BlockEntityFirepit bef = api.World.BlockAccessor.GetBlockEntity(pos) as BlockEntityFirepit;
            if (bef != null && !bef.canIgniteFuel)
            {
                bef.canIgniteFuel = true;
                bef.extinguishedTotalHours = api.World.Calendar.TotalHours;
            }
        }

        // reference:
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/Block/BlockPitkiln.cs#L274
        public void TryIgnitePitkiln(BlockPos pos)
        {
            BlockEntityPitKiln beb = api.World.BlockAccessor.GetBlockEntity(pos) as BlockEntityPitKiln;
            // beb?.TryIgnite((byEntity as EntityPlayer).Player);
            beb?.TryIgnite(null); // skipping player input
        }

        // reference:
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/Block/BlockForge.cs#L160
        public void TryIgniteForge(BlockPos pos)
        {
            BlockEntityForge bea = api.World.BlockAccessor.GetBlockEntity(pos) as BlockEntityForge;
            if (bea == null) return;
            var igniteMethod = bea.GetType().GetMethod("TryIgnite", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            igniteMethod.Invoke(bea, new object[] { });
        }

        // reference:
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/Block/BlockBloomery.cs#L117
        public void TryIgniteBloomery(BlockPos pos)
        {
            BlockEntityBloomery beb = api.World.BlockAccessor.GetBlockEntity(pos) as BlockEntityBloomery;
            beb?.TryIgnite();
        }

        // reference:
        // https://github.com/anegostudios/vssurvivalmod/blob/master/BlockEntity/BEAnvil.cs
        public void TryHeatAnvil(BlockPos pos)
        {
            BlockEntityAnvil bea = api.World.BlockAccessor.GetBlockEntity(pos) as BlockEntityAnvil;
            if (bea == null) return;

            var workItemStack = bea.WorkItemStack;
            if (workItemStack == null) return;

            float currTemperature = workItemStack.Collectible.GetTemperature(api.World, workItemStack);
            float maxTemperature = GameMath.Max(currTemperature, Config.KirinFireAnvilTemperatureMax);
            float newTemperature = GameMath.Clamp(currTemperature + Config.KirinFireAnvilTemperatureAdd, 0, maxTemperature);
            workItemStack.Collectible.SetTemperature(api.World, workItemStack, newTemperature);
            bea.MarkDirty();
        }
    }
}