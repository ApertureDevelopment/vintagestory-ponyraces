using System;
using HarmonyLib;
using Vintagestory.API.MathTools;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Common;

namespace ponyraces
{
    [HarmonyPatch(typeof(Vintagestory.GameContent.EntityBehaviorHealth))]
    [HarmonyPatch("OnFallToGround")]
    class PatchBehaviorHealthOnFallToGround
    {
        // modifying:
        // https://github.com/anegostudios/vsessentialsmod/blob/7234a11adea32a44127b356ad715bf29d21a53ed/Entity/Behavior/BehaviorHealth.cs#L227
        // add fall damage reduction
        static bool Prefix(Vintagestory.GameContent.EntityBehaviorHealth __instance, ref Vec3d positionBeforeFalling, ref double withYMotion)
        {
            var entity = __instance.entity;
            
            // THIS DOES NOT work that well, players complain about fall
            // damage being retarded and randomly doing massive damage
            // to you for like a 1 block drop x,x 
            // // fall damage reduction by modiftying yDistance using fall damage
            // // multiplier. its retarded but works okay idk lol
            // float fallDamageMultiplier = 2f - entity.Stats.GetBlended("fallResistance");
            // fallDamageMultiplier = GameMath.Max(0f, fallDamageMultiplier); // make sure not negative

            // double yDistance = Math.Abs(positionBeforeFalling.Y - entity.Pos.Y);
            // positionBeforeFalling.Y = entity.Pos.Y + yDistance * fallDamageMultiplier;

            // return true; // run original

            // SO INSTEAD just copy-pasting original code and adding in the
            // fall damage modifier :^(

            if (!entity.Properties.FallDamage) return false;
            bool gliding = (entity as EntityAgent)?.ServerControls.Gliding == true;

            double yDistance = Math.Abs(positionBeforeFalling.Y - entity.Pos.Y);

            if (yDistance < 3.5f) return false;
            if (gliding)
            {
                yDistance = Math.Min(yDistance / 2, Math.Min(14, yDistance));
                withYMotion /= 2;

                // 1.5x pi is down
                // 1 x pi is horizontal
                // 0.5x pi half is up
                if (entity.ServerPos.Pitch < 1.25 * GameMath.PI)
                {
                    yDistance = 0;
                }
            }

            // Experimentally determined - at 3.5 blocks the player has a motion of -0.19
            if (withYMotion > -0.19) return false;  

            double fallDamage = Math.Max(0, yDistance - 3.5f);

            float fallDamageMultiplier = 2f - entity.Stats.GetBlended("fallResistance");
            fallDamage = fallDamage * fallDamageMultiplier;

            // Some super rough experimentally determined formula that always underestimates
            // the actual ymotion.
            // lets us reduce the fall damage if the player lost in y-motion during the fall
            // will totally break if someone changes the gravity constant
            double expectedYMotion = -0.041f * Math.Pow(fallDamage, 0.75f) - 0.22f;
            double yMotionLoss = Math.Max(0, -expectedYMotion + withYMotion);
            fallDamage -= 20 * yMotionLoss;

            if (fallDamage <= 0) return false;

            /*if (fallDamage > 2)
            {
                entity.StartAnimation("heavyimpact");
            }*/

            entity.ReceiveDamage(new DamageSource()
            {
                Source = EnumDamageSource.Fall,
                Type = EnumDamageType.Gravity
            }, (float)fallDamage);

            return false; // skip original
        }
    }
}
