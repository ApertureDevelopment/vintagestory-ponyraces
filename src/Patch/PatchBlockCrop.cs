using System;
using System.Collections.Generic;
using HarmonyLib;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;


namespace ponyraces
{
    [HarmonyPatch(typeof(Vintagestory.GameContent.BlockCrop))]
    [HarmonyPatch("GetDrops")]
    class PatchBlockCropGetDrops
    {
        static void Postfix(IWorldAccessor world, IPlayer byPlayer, ref ItemStack[] __result)
        {
            if (byPlayer != null)
            {
                float playerDropRate = byPlayer.Entity.Stats.GetBlended("cropDropRate");
                float dropRateEffect = world.Api.ModLoader.GetModSystem<PonyRaceMod>()?.Config.CropDropRateEffect ?? 1.0f;
                float cropDropRate = Math.Max(0f, 1f + (playerDropRate - 1f) * dropRateEffect);

                if (cropDropRate != 1.0)
                {
                    List<ItemStack> newDrops = new List<ItemStack>(__result.Length);
                    foreach (var drop in __result)
                    {
                        if (!(drop.Item is Vintagestory.GameContent.ItemPlantableSeed))
                        {
                            drop.StackSize = GameMath.RoundRandom(world.Rand, cropDropRate * drop.StackSize);
                        }

                        if (drop.StackSize > 0) newDrops.Add(drop);
                    }

                    __result = newDrops.ToArray();
                }
            }
        }
    }
}