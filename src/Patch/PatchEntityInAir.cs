using HarmonyLib;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;


namespace ponyraces
{
    [HarmonyPatch(typeof(Vintagestory.GameContent.EntityInAir))]
    [HarmonyPatch("DoApply")]
    class PatchEntityInAirApplyFlying
    {
        // modifying:
        // https://github.com/anegostudios/vsessentialsmod/blob/ca951574506b6382be1ffb8b68dac20b7ea2af48/Entity/EntityLocomotion/EntityInAir.cs#L60
        static bool Prefix(ref float dt, ref Entity entity, ref EntityPos pos, ref EntityControls controls)
        {
            if (controls.IsFlying)
            {
                double deltaY = controls.FlyVector.Y;
                if (controls.Up || controls.Down)
                {
                    float moveSpeed = dt * GlobalConstants.BaseMoveSpeed * controls.MovespeedMultiplier / 2;
                    deltaY = (controls.Up ? moveSpeed : 0) + (controls.Down ? -moveSpeed : 0);
                }

                // Prevent entities from flying too close to dimension boundaries
                // (e.g. capped at 24k height in the normal world, with first dimension boundary at 32k)
                if (deltaY > 0 && pos.Y % BlockPos.DimensionBoundary > BlockPos.DimensionBoundary * 3 / 4) deltaY = 0;

                // multiplier for flying speed
                float speedMultiplier = entity.Stats.GetBlended("walkspeed");

                pos.Motion.Add(speedMultiplier * controls.FlyVector.X, deltaY, speedMultiplier * controls.FlyVector.Z);

                return false; // skip original
            }

            return true; // run original
        }
    }
}
