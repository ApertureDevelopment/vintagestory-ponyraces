using System;
using HarmonyLib;
using Vintagestory.API.Common;


namespace ponyraces
{
    [HarmonyPatch(typeof(Vintagestory.API.Common.EntityPlayer))]
    [HarmonyPatch("get_LightHsv")]
    class PatchEntityPlayerLightHsv
    {
        // modifying:
        // https://github.com/anegostudios/vsapi/blob/master/Common/Entity/EntityPlayer.cs#L218
        static void Postfix(EntityPlayer __instance, ref byte[] __result)
        {
            if (__instance.Player?.WorldData.CurrentGameMode == EnumGameMode.Spectator) return;

            var light = __instance.GetBehavior<EntityBehaviorPonyLight>();
            // Console.WriteLine($"PatchEntityPlayerLightHsv Postfix, light.Enabled? = {light?.Enabled}");
            if (light != null && light.Enabled)
            {
                // Console.WriteLine("PatchEntityPlayerLightHsv Postfix, mixing light");
                if (__result != null) // mix together lights
                {
                    // mix hue and sat of two lights
                    byte[] lightHsv = light.LightHsv;
                    float totalval = __result[2] + light.LightHsv[2];
                    float t = __result[2] / totalval;
                    __result = new byte[] {
                        (byte)(__result[0] * t + lightHsv[0] * (1-t)), // lerp
                        (byte)(__result[1] * t + lightHsv[1] * (1-t)), // lerp
                        // make value the brighter of the two lights
                        Math.Max(__result[2], lightHsv[2])
                    };
                }
                else
                {
                    __result = light.LightHsv;
                }
            }
        }
    }

    [HarmonyPatch(typeof(Vintagestory.API.Common.EntityPlayer))]
    [HarmonyPatch("ShouldReceiveDamage")]
    class PatchEntityPlayerShouldReceiveDamage
    {
        // modifying:
        // https://github.com/anegostudios/vsapi/blob/ae41df550efe3b64508c615479ee30db108af58f/Common/Entity/EntityPlayer.cs#L1113
        static bool Prefix(EntityPlayer __instance, DamageSource damageSource, float damage, ref bool __result)
        {
            if (damageSource.Type == EnumDamageType.Fire)
            {
                // fire resistance must be >1 to be immune to fire
                float fireResist = __instance.Stats.GetBlended("fireResistance");
                if (fireResist > 1)
                {
                    __result = false;
                    return false; // skip original
                }
            }

            return true; // run original
        }
    }
}