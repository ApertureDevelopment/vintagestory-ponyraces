using System.Diagnostics;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace ponyraces
{
    /// <summary>
    /// Contains bucking related functions.
    /// </summary>
    public partial class PonyRaceMod
    {
        /// <summary>
        /// Hotkey handler for horse bucking.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public bool OnHotkeyBuck(KeyCombination keys)
        {
            var player = capi.World.Player;

            // client-side check if can use
            // nvm, allow all players to do animation, but customize effects
            // on server side
            // bool canBuck = kemono.HasAnyRaceTrait(player.Entity, BuckAllowedTraits);
            // if (!canBuck)
            // {
            //     Debug.WriteLine("[Client] CANNOT BUCK");
            //     return true;
            // }

            var staminaBehavior = player.Entity.GetBehavior<EntityBehaviorPonyStamina>();
            if (staminaBehavior != null)
            {
                if (staminaBehavior.CurrentStamina < 1) return true;

                // set current client side stamina to 0 to prevent spam
                staminaBehavior.CurrentStamina = 0;
            }

            // send packet to server requesting to buck
            clientToServerChannel.SendPacket(new PacketClientRequestBuck());

            return true;
        }

        /// <summary>
        /// Server packet handler for player bucking request.
        /// TODO: clean this shit up
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveRequestBuck(IServerPlayer fromPlayer, PacketClientRequestBuck p)
        {
            var entity = fromPlayer.Entity;

            // play buck animation
            // WHY THE FUCK DOESNT THIS WORK?
            // entity.AnimManager.StartAnimation(new AnimationMetaData() { Animation = "buck", Code = "buck", AnimationSpeed = 1f }.Init());
            // have to manually send packet to client and force animation...
            serverToClientChannel.BroadcastPacket(new PacketServerStartAnimation
            {
                EntityId = entity.EntityId,
                Code = "buck",
            });

            // use up stamina
            var staminaBehavior = entity.GetBehavior<EntityBehaviorPonyStamina>();
            if (staminaBehavior != null)
            {
                staminaBehavior.CurrentStamina = 0f;
            }

            // effect 1: unmount any passenger
            var flight = entity.GetBehavior<EntityBehaviorPonyFlight>();
            if (flight != null && flight.MountEntity != null)
            {
                flight.MountEntity.RemovePassenger();
                flight.MountEntity.Die(EnumDespawnReason.Removed);
            }

            // effect 2: bucking block, do server-side check if can buck block
            bool canBuck = kemono.HasAnyRaceTrait(entity, BuckAllowedTraits);
            if (canBuck)
            {
                // handle bucking block
                TryBuckBlock(entity);
            }
            else // just plays funny sound
            {
                api.Event.RegisterCallback((dt) => 
                    entity.World.PlaySoundAt(
                        new AssetLocation("ponyraces:sounds/pony/bucknothing"),
                        entity,
                        null,
                        false,
                        20,
                        0.8f
                    )
                , 100);
            }
        }

        /// <summary>
        /// Make pony buck block behind it. Cancels if pony does not have 
        /// trait requirements or if block is not buckable.
        /// </summary>
        /// <param name="entity"></param>
        public void TryBuckBlock(Entity entity)
        {
            if (api.Side != EnumAppSide.Server) return;

            // project behind player by up to 1 block to find tree
            Vec3d behindDirection = MathUtils.Vec3dFromYaw(entity.SidedPos.Yaw).Mul(-1);
            Vec3d behindPos = entity.SidedPos.XYZ.AddCopy(behindDirection);
            BlockPos position = new BlockPos((int)behindPos.X, (int)behindPos.Y, (int)behindPos.Z, entity.Pos.Dimension);
            Block block = entity.World.BlockAccessor.GetBlock(position);

            // schedule handling based on block type
            bool isBucking = HandleBuckBlockType(entity, block, position);
            if (isBucking == false)
            {
                api.Event.RegisterCallback((dt) => 
                    entity.World.PlaySoundAt(
                        new AssetLocation("ponyraces:sounds/pony/bucknothing"),
                        entity,
                        null,
                        false,
                        20,
                        0.8f
                    )
                , 100);
                
                if (entity is EntityPlayer)
                {
                    var player = (entity as EntityPlayer).Player as IServerPlayer;
                    player.SendIngameError("bucknothing", Lang.Get("ponyraces:msg-error-buck-nothing"));
                }
                return;
            }
        }

        /// <summary>
        /// Schedules delayed task to finish handling bucking specific block.
        /// Returns true if valid block is being bucked, false otherwise.
        /// Split into separate function so harmony patch can postfix this
        /// for custom handling of different block types.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        public bool HandleBuckBlockType(Entity entity, Block block, BlockPos position)
        {
            Debug.WriteLine($"HandleBuckBlockType {entity} {block} {position}");
            
            if (block.Code == null || block.Id == 0) return false; // skip air

            // this is a block, play buck sound
            // (delayed handling to sync with buck animation to be cooler)
            api.Event.RegisterCallback((dt) => 
                entity.World.PlaySoundAt(
                    new AssetLocation("ponyraces:sounds/pony/bucktree"),
                    entity,
                    null,
                    false,
                    50,
                    1f
                )
            , 200);

            // permissions check to allow bucking here
            if (entity is EntityPlayer)
            {
                var player = (entity as EntityPlayer).Player as IServerPlayer;
                EnumWorldAccessResponse access = entity.World.Claims.TestAccess(player, position, EnumBlockAccessFlags.BuildOrBreak);
                {
                    if (access != EnumWorldAccessResponse.Granted)
                    {
                        player.SendIngameError("buckpermission", Lang.Get("ponyraces:msg-error-buck-no-permission"));
                        return true;
                    }
                }
            }

            // schedule handling
            // (delayed handling to sync with buck animation to look cooler)
            if (block is BlockFruitTreePart)
            {
                entity.GetBehavior<EntityBehaviorHunger>()?.ConsumeSaturation(Config.BuckFruitTreeHunger);
                api.Event.RegisterCallback((dt) => FinishBuckFruitTree(position), 500);
            }
            else if (block is BlockBerryBush)
            {
                entity.GetBehavior<EntityBehaviorHunger>()?.ConsumeSaturation(Config.BuckBerryBushHunger);
                api.Event.RegisterCallback((dt) => FinishBuckBerryBush(entity, position), 400);
            }
            else if (block is BlockLog)
            {
                entity.GetBehavior<EntityBehaviorHunger>()?.ConsumeSaturation(Config.BuckTreeHunger);
                api.Event.RegisterCallback((dt) => FinishBuckRegularTree(entity, position), 500);
            }
            else
            {
                return true;
            }

            return true;
        }

        // references:
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/Systems/FruitTree/BlockFruitTreeBranch.cs
        public void FinishBuckFruitTree(BlockPos buckPosition)
        {
            // search upwards for all fruit tree blocks to harvest
            int byStart = buckPosition.Y + 1;
            int byEnd = buckPosition.Y + 6;

            // fruit tree area size is 3x3 square ingame
            int bx0 = buckPosition.X;
            int bz0 = buckPosition.Z;

            int bxStart = bx0 - 2;
            int bxEnd = bx0 + 2;
            int bzStart = bz0 - 2;
            int bzEnd = bz0 + 2;

            for (int by = byStart; by < byEnd; by += 1)
            {
                // flag to check if all blocks in layer are air
                // if so, can early exit this loop
                bool allAir = true;

                for (int bx = bxStart; bx <= bxEnd; bx += 1)
                {
                    for (int bz = bzStart; bz <= bzEnd; bz += 1)
                    {
                        var pos = new BlockPos(bx, by, bz, buckPosition.dimension);
                        var block = api.World.BlockAccessor.GetBlock(pos);
                        if (block.Code == null || block.Id == 0) continue; // skip air
                        
                        // must not be air block
                        allAir = false;

                        var be = api.World.BlockAccessor.GetBlockEntity(pos);
                        if (be is not BlockEntityFruitTreePart) continue;

                        var fruitPart = be as BlockEntityFruitTreePart;
                        if (fruitPart.FoliageState != EnumFoliageState.Ripe) continue;

                        fruitPart.FoliageState = EnumFoliageState.Plain;
                        fruitPart.MarkDirty(true);
                        // fruitPart.harvested = true;
                        // use reflection to set harvested (protected)
                        var harvestedProp = fruitPart.GetType().GetField("harvested", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                        harvestedProp.SetValue(fruitPart, true);

                        var fruitAsset = AssetLocation.Create(fruitPart.Block.Attributes["branchBlock"].AsString(), fruitPart.Block.Code.Domain);
                        var fruitBlock = api.World.GetBlock(fruitAsset) as BlockFruitTreeBranch;
                        var drops = fruitBlock.TypeProps[fruitPart.TreeType].FruitStacks;

                        // find drop location: look for closest air block below 
                        // fruit block part

                        foreach (var drop in drops)
                        {
                            ItemStack stack = drop.GetNextItemStack(1);
                            if (stack == null) continue;

                            api.World.SpawnItemEntity(stack, new Vec3d(bx + 0.5, by, bz + 0.5));

                            if (drop.LastDrop) break;
                        }
                    }
                }

                if (allAir) break;
            }

        }

        // references:
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/Block/BlockBerryBush.cs
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/BlockEntity/BEBerryBush.cs
        // https://github.com/anegostudios/vssurvivalmod/blob/5a5528e1e95325741537ef152c813a8a69acd890/BlockBehavior/BehaviorHarvestable.cs
        public void FinishBuckBerryBush(Entity entity, BlockPos buckPosition)
        {
            // search upwards for all berry bush blocks to harvest
            int byStart = buckPosition.Y;
            int byEnd = buckPosition.Y + 2;

            // do a 3x3 harvest area
            int bx0 = buckPosition.X;
            int bz0 = buckPosition.Z;

            int bxStart = bx0 - 1;
            int bxEnd = bx0 + 1;
            int bzStart = bz0 - 1;
            int bzEnd = bz0 + 1;

            for (int by = byStart; by <= byEnd; by += 1)
            {
                // flag to check if all blocks in layer are air
                // if so, can early exit this loop
                bool allAir = true;

                for (int bx = bxStart; bx <= bxEnd; bx += 1)
                {
                    for (int bz = bzStart; bz <= bzEnd; bz += 1)
                    {
                        var pos = new BlockPos(bx, by, bz, buckPosition.dimension);
                        var block = api.World.BlockAccessor.GetBlock(pos);
                        if (block.Code == null || block.Id == 0) continue; // skip air

                        // must not be air block
                        allAir = false;

                        // if not berry bush continue
                        if (block is not BlockBerryBush) continue;
                        
                        // get harvest behavior
                        var bbh = block.GetBehavior<BlockBehaviorHarvestable>();
                        if (bbh?.harvestedStack == null) continue; // occur if block not ripe

                        float dropRate = 1;

                        if (block.Attributes?.IsTrue("forageStatAffected") == true)
                        {
                            dropRate *= entity.Stats.GetBlended("forageDropRate");
                        }

                        ItemStack stack = bbh.harvestedStack.GetNextItemStack(dropRate);
                        if (stack == null) continue;

                        api.World.SpawnItemEntity(stack, pos.ToVec3d().Add(0.5, 0.5, 0.5));

                        var harvestedBlockField = bbh.GetType().GetField("harvestedBlock", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                        if (harvestedBlockField != null)
                        {
                            var harvestedBlock = harvestedBlockField.GetValue(bbh);
                            if (harvestedBlock != null && harvestedBlock is Block)
                            {
                                api.World.BlockAccessor.SetBlock((harvestedBlock as Block).BlockId, pos);
                            }
                        }
                    }
                }

                if (allAir) break;
            }
        }

        /// <summary>
        /// Handler for bucking a normal tree. Search upwards and randomly 
        /// harvest leaves for sticks and saplings. Scaling drop rate based on
        /// player foraging ability.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="buckPosition"></param>
        public void FinishBuckRegularTree(Entity entity, BlockPos buckPosition)
        {
            // player stick and sapling drop rate, scale by foraging
            float dropRate = 1;
            dropRate *= entity.Stats.GetBlended("forageDropRate");

            // stick item
            var stickItem = api.World.GetItem(new AssetLocation("stick"));

            // max search upwards along tree
            int byStart = buckPosition.Y;
            int byEnd = buckPosition.Y + 10;

            // current log location.
            // since some trees logs branch out, we want to update expected
            // tree center on each layer. for each layer, we search around a
            // 5x5 area surrounding the previous layer's log location.  
            // we update this center to the closest log found in previous layer.
            int bx0 = buckPosition.X;
            int bz0 = buckPosition.Z;

            for (int by = byStart; by < byEnd; by += 1)
            {
                // flag to check if all blocks in layer are air
                // if so, can early exit this loop
                bool allAir = true;

                // center search around this log location
                int bxStart = bx0 - 2;
                int bxEnd = bx0 + 2;
                int bzStart = bz0 - 2;
                int bzEnd = bz0 + 2;

                bool nextLogFound = false;
                double nextLogDistSq = 0.0;
                int nextBx0 = bx0;
                int nextBz0 = bz0;

                for (int bx = bxStart; bx <= bxEnd; bx += 1)
                {
                    for (int bz = bzStart; bz <= bzEnd; bz += 1)
                    {
                        var pos = new BlockPos(bx, by, bz, buckPosition.dimension);
                        var block = api.World.BlockAccessor.GetBlock(pos);
                        if (block.Code == null || block.Id == 0) continue; // skip air

                        // must not be air block
                        allAir = false;

                        // block log: update next log location
                        if (block is BlockLog)
                        {
                            if (nextLogFound == false)
                            {
                                nextLogFound = true;
                                int dx = bx - bx0;
                                int dz = bz - bz0;
                                nextLogDistSq = dx * dx + dz * dz;
                                nextBx0 = bx;
                                nextBz0 = bz;
                            }
                            else
                            {
                                int dx = bx - bx0;
                                int dz = bz - bz0;
                                double distSq = dx * dx + dz * dz;
                                if (distSq < nextLogDistSq)
                                {
                                    nextLogDistSq = distSq;
                                    nextBx0 = bx;
                                    nextBz0 = bz;
                                }
                            }
                        }
                        
                        // leaves: handle stick and sapling drop
                        if (block is BlockLeaves)
                        {
                            double rng = api.World.Rand.NextDouble();
                            bool drop = false;

                            // sapling drop
                            if (rng < Config.BuckTreeSaplingDropRate)
                            {
                                drop = true;
                                var seedItem = api.World.GetItem(AssetLocation.Create("treeseed-" + block.Variant["wood"], block.Code.Domain));
                                if (seedItem != null)
                                {
                                    ItemStack stack = new ItemStack(seedItem, 1);
                                    api.World.SpawnItemEntity(stack, pos.ToVec3d().Add(0.5, 0.5, 0.5));
                                }
                            }

                            // stick drop
                            if (rng < Config.BuckTreeStickDropRate)
                            {
                                drop = true;
                                if (stickItem != null)
                                {
                                    ItemStack stack = new ItemStack(stickItem, 1);
                                    api.World.SpawnItemEntity(stack, pos.ToVec3d().Add(0.5, 0.5, 0.5));
                                }
                            }

                            if (drop)
                            {
                                // turn leaves to air
                                api.World.BlockAccessor.SetBlock(0, pos);
                            }
                        }
                    }
                }

                if (allAir) break;

                bx0 = nextBx0;
                bz0 = nextBz0;
            }
        }
    }
}
