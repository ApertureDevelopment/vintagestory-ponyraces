using Vintagestory.API.Client;
using Vintagestory.API.Common;

namespace ponyraces
{
    // based on:
    // https://github.com/squ1b3r/vs-oxygen-not-included/blob/main/src/Gui/HudElementAirBar.cs

    /// <summary>
    /// HUD stamina bar shared for custom pony abilities (other than flight).
    /// </summary>
    public class HudElementStamina : HudElement
    {
        public const float StatsBarParentWidth = 200f;
        public const float StatsBarWidth = StatsBarParentWidth * 0.41f;
        public static double[] StatBarColor = {0.6, 0.6, 0.6, 0.5};

        private GuiElementStatbar _statbar;

        public override double InputOrder => 1.0;

        public HudElementStamina(ICoreClientAPI capi) : base(capi)
        {
            capi.Event.RegisterGameTickListener(OnGameTick, 20);
        }

        private void OnGameTick(float dt)
        {
            Update();
        }

        public void Update()
        {
            if (_statbar == null) return;

            var bh = capi.World.Player.Entity.GetBehavior<EntityBehaviorPonyStamina>();

            if (bh == null) return;

            float currentStamina = bh.CurrentStamina;
            float maxStamina = bh.MaxStamina;

            _statbar.SetValues(currentStamina, 0.0f, maxStamina);
        }

        private void ComposeGuis()
        {
            var statsBarBounds = new ElementBounds()
            {
                Alignment = EnumDialogArea.CenterBottom,
                BothSizing = ElementSizing.Fixed,
                fixedWidth = StatsBarParentWidth,
                fixedHeight = 100.0
            }.WithFixedAlignmentOffset(0.0, 5.0);

            var alignment = EnumDialogArea.CenterBottom;

            var statBarBounds = ElementStdBounds.Statbar(alignment, StatsBarWidth)
                .WithFixedAlignmentOffset(0, -200)
                .WithFixedHeight(10.0);

            var statBarParentBounds = statsBarBounds.FlatCopy().FixedGrow(0.0, 20.0);

            var composer = capi.Gui.CreateCompo("ponystamina", statBarParentBounds);

            _statbar = new GuiElementStatbar(composer.Api, statBarBounds, StatBarColor, false, true);
            _statbar.HideWhenFull = true;

            composer
                .BeginChildElements(statsBarBounds)
                .AddInteractiveElement(_statbar, "ponystamina")
                .EndChildElements()
                .Compose();

            Composers["ponystamina"] = composer;

            // must set values after compose
            _statbar.SetValues(1, 0, 1);

            TryOpen();
        }

        public override void OnOwnPlayerDataReceived()
        {
            ComposeGuis();
            Update();
        }

        public override void OnRenderGUI(float deltaTime)
        {
            if (capi.World.Player.WorldData.CurrentGameMode == EnumGameMode.Spectator) return;

            base.OnRenderGUI(deltaTime);
        }

        public override bool TryClose() => false;

        public override bool ShouldReceiveKeyboardEvents() => false;

        public override bool Focusable => false;
    }
}
