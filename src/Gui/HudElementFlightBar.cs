using Vintagestory.API.Client;
using Vintagestory.API.Common;

namespace ponyraces
{
    // based on:
    // https://github.com/squ1b3r/vs-oxygen-not-included/blob/main/src/Gui/HudElementAirBar.cs
    public class HudElementFlightBar : HudElement
    {
        public const float StatsBarParentWidth = 850f;
        public const float StatsBarWidth = StatsBarParentWidth * 0.41f;
        public static double[] StatBarColor = {1.0, 0.6, 0.2, 0.5};

        private GuiElementStatbar _statbar;

        public override double InputOrder => 1.0;

        public HudElementFlightBar(ICoreClientAPI capi) : base(capi)
        {
            capi.Event.RegisterGameTickListener(OnGameTick, 20);
        }

        private void OnGameTick(float dt)
        {
            Update();
        }

        public void Update()
        {
            if (_statbar == null) return;

            var flight = capi.World.Player.Entity.GetBehavior<EntityBehaviorPonyFlight>();

            if (flight == null || !flight.CanFly) return;

            float currentStamina = flight.CurrentStamina;
            float maxStamina = flight.MaxStamina;

            var lineInterval = maxStamina * 0.1f;

            _statbar.SetLineInterval(lineInterval);
            _statbar.SetValues(currentStamina, 0.0f, maxStamina);

            float flashingThreshold = maxStamina * 0.2f;
            _statbar.ShouldFlash = flashingThreshold > 0 && currentStamina < flashingThreshold;
        }

        private void ComposeGuis()
        {
            var flight = capi.World.Player.Entity.GetBehavior<EntityBehaviorPonyFlight>();

            var statsBarBounds = new ElementBounds()
            {
                Alignment = EnumDialogArea.CenterBottom,
                BothSizing = ElementSizing.Fixed,
                fixedWidth = StatsBarParentWidth,
                fixedHeight = 100.0
            }.WithFixedAlignmentOffset(0.0, 5.0);

            var alignment = EnumDialogArea.RightTop;

            var statBarBounds = ElementStdBounds.Statbar(alignment, StatsBarWidth)
                .WithFixedAlignmentOffset(-2, -16)
                .WithFixedHeight(10.0);

            var statBarParentBounds = statsBarBounds.FlatCopy().FixedGrow(0.0, 20.0);

            var composer = capi.Gui.CreateCompo("ponyflight", statBarParentBounds);

            _statbar = new GuiElementStatbar(composer.Api, statBarBounds, StatBarColor, true, true);
            _statbar.HideWhenFull = true;

            composer
                .BeginChildElements(statsBarBounds)
                // .AddIf(flight.CanFly)
                .AddInteractiveElement(_statbar, "ponyflightbar")
                // .EndIf()
                .EndChildElements()
                .Compose();

            Composers["ponyflight"] = composer;

            // must set values after compose
            _statbar.SetValues(flight.MaxStamina, 0.0f, flight.MaxStamina);

            TryOpen();
        }

        public override void OnOwnPlayerDataReceived()
        {
            ComposeGuis();
            Update();
        }

        public override void OnRenderGUI(float deltaTime)
        {
            if (capi.World.Player.WorldData.CurrentGameMode == EnumGameMode.Spectator) return;

            base.OnRenderGUI(deltaTime);
        }

        public override bool TryClose() => false;

        public override bool ShouldReceiveKeyboardEvents() => false;

        public override bool Focusable => false;
    }
}
