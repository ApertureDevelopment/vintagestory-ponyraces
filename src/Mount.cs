using ProtoBuf;
using System;
using System.Diagnostics;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace ponyraces
{
    // apparently nothing has actually changed in mounting since xrowboat,
    // idk how vanilla mounting works since nothing is actually properly
    // synchronized from client to server, e.g. unmounting doesnt trigger
    // on server or on other clients.
    // 
    // so we have to reimplement sync just like xrowboat, its over :^(
    // https://gitlab.com/xeth/vintagestory-rowboat/-/blob/master/src/XRowboat.cs

    /// <summary>
    /// Packet from client -> server requesting mounting the entity
    /// HostEntityId.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientMount
    {
        // host horse entity id to mount
        public long HostEntityId;
    }
    
    /// <summary>
    /// Packet from client -> server notifying player is unmounting the
    /// entity from HostEntityId.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientUnmount {
        // EntityPonyMount glue to kill when unmounting
        public long MountEntityId;
    }

    /// <summary>
    /// Packet from client -> server notifying server that player is
    /// enabling mountable state.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientMountableStart
    {

    }

    /// <summary>
    /// Packet from client -> server notifying server that player is
    /// stopping mountable state.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientMountableStop
    {

    }

    /// <summary>
    /// Contains horse mounting synchronization.
    /// </summary>
    public partial class PonyRaceMod
    {

        // client mounting state:

        // player is allowed to be mounted
        bool MountingEnabled = false;

        /// <summary>
        /// Sends mount request to server to mount host horse.
        /// </summary>
        /// <param name="hostEntityId"></param>
        public void ClientMountRequest(long hostEntityId)
        {
            clientToServerChannel.SendPacket(new PacketClientMount()
            {
                HostEntityId = hostEntityId
            });
        }

        /// <summary>
        /// Sends unmount request to server to unmount the mount helper.
        /// </summary>
        /// <param name="mountEntityId"></param>
        public void ClientUnmountRequest(long mountEntityId)
        {
            clientToServerChannel.SendPacket(new PacketClientUnmount()
            {
                MountEntityId = mountEntityId
            });
        }
        
        /// <summary>
        /// Handles mount by creating a helper EntityPonyMount entity.
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveMount(IPlayer fromPlayer, PacketClientMount p)
        {
            Entity host = sapi.World.GetEntityById(p.HostEntityId);
            if (host != null)
            {
                var world = sapi.World;
                var spawnPos = host.ServerPos.XYZ;

                // check host mountable state
                var flight = host.GetBehavior<EntityBehaviorPonyFlight>();
                if (flight == null || flight.Mountable == false)
                {
                    return;
                }

                // create mount attach entity
                var type = world.GetEntityType(new AssetLocation("ponyraces:ponymount"));
                var mount = world.ClassRegistry.CreateEntity(type) as EntityPonyMount;
                mount.ServerPos.SetFrom(spawnPos); // SidedPos crashes here, server/client side not set yet?
                mount.Host = host;
                mount.HostPonyFlight = host.GetBehavior<EntityBehaviorPonyFlight>();
                mount.Passenger = fromPlayer.Entity;
                fromPlayer.Entity.TryMount(mount);
                world.SpawnEntity(mount);
            }
        }

        /// <summary>
        /// Handles unmount by killing the mount helper EntityPonyMount
        /// entity
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveUnmount(IPlayer fromPlayer, PacketClientUnmount p)
        {
            Entity mount = sapi.World.GetEntityById(p.MountEntityId);
            if (mount != null)
            {
                fromPlayer.Entity.TryUnmount();
                mount.Die(EnumDespawnReason.Removed);
            }
        }


        /// <summary>
        /// Hotkey handler to allow player to be mountable.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public bool OnHotkeyMountable(KeyCombination keys)
        {
            if (MountingEnabled == false)
            {
                MountingEnabled = true;
                
                // send packet to server requesting to enable mountable state
                clientToServerChannel.SendPacket(new PacketClientMountableStart());
            }
            //// FOR DEBUGGING: this enables toggle mode
            // else
            // {
            //     MountingEnabled = false;

            //     // send packet to server requesting to disable mountable state
            //     clientToServerChannel.SendPacket(new PacketClientMountableStop());
            // }

            return true;
        }

        /// <summary>
        /// Runs each tick to check if client player stopped holding the
        /// mountable hotkey.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public void OnClientStopMountable(float dt)
        {
            // return; // for debugging, disables this function to use toggle mode instead

            // check if mounting hotkey no longer pressed
            if (MountingEnabled == true)
            {
                var mountingHotkey = capi.Input.GetHotKeyByCode("ponymountable");
                int key = mountingHotkey.CurrentMapping.KeyCode;
                
                if (!capi.Input.KeyboardKeyStateRaw[key])
                {
                    MountingEnabled = false;
                    Console.WriteLine("Mountable disabled");

                    // send packet to server requesting to disable mountable state
                    clientToServerChannel.SendPacket(new PacketClientMountableStop());
                }
            }
        }


        /// <summary>
        /// Server handler to allow player to be mountable.
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveMountableStart(IPlayer fromPlayer, PacketClientMountableStart p)
        {
            Entity entity = fromPlayer.Entity;

            // set player mountable mountable state
            var flight = entity.GetBehavior<EntityBehaviorPonyFlight>();
            if (flight != null)
            {
                flight.Mountable = true;
            }

            // play animation
            serverToClientChannel.BroadcastPacket(new PacketServerStartAnimation
            {
                EntityId = entity.EntityId,
                Code = "mountallow",
            });
        }

        /// <summary>
        /// Server handler to stop player from being mountable.
        /// entity
        /// </summary>
        /// <param name="fromPlayer"></param>
        /// <param name="p"></param>
        public void OnServerReceiveMountableStop(IPlayer fromPlayer, PacketClientMountableStop p)
        {
            Entity entity = fromPlayer.Entity;

            // set player mountable mountable state
            var flight = entity.GetBehavior<EntityBehaviorPonyFlight>();
            if (flight != null)
            {
                flight.Mountable = false;
            }

            serverToClientChannel.BroadcastPacket(new PacketServerStopAnimation
            {
                EntityId = entity.EntityId,
                Code = "mountallow",
            });
        }
    }
}