using System;
using System.IO;
using System.Linq;
using ponyraces;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace ponyraces
{
    // based on:
    // https://github.com/anegostudios/vssurvivalmod/blob/2bfd75697208df8f1d5386e117f7f791df596cc6/Systems/Boats/EntityPonyMount.cs
    // https://github.com/anegostudios/vssurvivalmod/blob/2bfd75697208df8f1d5386e117f7f791df596cc6/Systems/Boats/BoatSeat.cs
    
    public class EntityPonyMount : Entity, IMountable, IMountableSupplier, IRenderer
    {

        public override bool ApplyGravity
        {
            get { return false; }
        }

        public override bool IsInteractable
        {
            get { return false; }
        }

        public IMountable[] MountPoints => Array.Empty<IMountable>();

        // host attach point for passenger mount
        string MountAttachPoint = "Back";

        ICoreClientAPI capi;

        // for IRenderer
        public double RenderOrder => 0.01; // makes this occur after normal renderers
        public int RenderRange => 999;

        // host entity this is attached to
        public Entity Host = null;
        public EntityBehaviorPonyFlight HostPonyFlight = null;
        public long HostEntityIdForInit;

        // entity mounted on this
        public EntityAgent Passenger = null;
        public long PassengerEntityIdForInit;

        public bool controllable;

        protected Vec3f eyePos = new Vec3f(0, 1, 0);

        public EntityControls controls = new EntityControls();

        public EntityControls Controls
        {
            get {
                return controls; 
            }
        }

        public static string AnimationWhenFlying = "mountponyflying";
        public static string AnimationOnGround = "mountponyground";

        public string SuggestedAnimation => HostPonyFlight?.IsFlying == true ? AnimationWhenFlying : AnimationOnGround;

        public IMountableSupplier MountSupplier => this;
        public EnumMountAngleMode AngleMode => EnumMountAngleMode.Push;
        public new Vec3f LocalEyePos => eyePos;
        public Entity MountedBy => Passenger;
        public bool CanControl => controllable;

        public EntityPonyMount()
        {
            controls.OnAction = OnControls;
        }

        public override void Initialize(EntityProperties properties, ICoreAPI api, long InChunkIndex3d)
        {            
            base.Initialize(properties, api, InChunkIndex3d);

            capi = api as ICoreClientAPI;
            if (capi != null)
            {
                capi.Event.RegisterRenderer(this, EnumRenderStage.Before, "ponymount");
            }

            // from vanilla:
            // "The mounted entity will try to mount as well, but at that time,
            // the boat might not have been loaded, so we'll try mounting on both ends.
            // (wtf?)
            if (HostEntityIdForInit != 0 && Host == null)
            {
                if (api.World.GetEntityById(HostEntityIdForInit) is EntityAgent entity)
                {
                    Host = entity;
                    HostPonyFlight = Host.GetBehavior<EntityBehaviorPonyFlight>();
                }
            }
            if (PassengerEntityIdForInit != 0 && Passenger == null)
            {
                if (api.World.GetEntityById(PassengerEntityIdForInit) is EntityAgent entity)
                {
                    entity.TryMount(this);
                }
            }

            // set host mount entity
            if (HostPonyFlight != null)
            {
                HostPonyFlight.MountEntity = this;
            }
        }

        public override void OnGameTick(float dt)
        {
            EntityPlayer hostPlayer = Host as EntityPlayer;
            bool hostIsOnline = hostPlayer?.Player != null;
            bool hostIsFlying = HostPonyFlight?.IsFlying == true;

            // conditions to remove this mount entity when no longer needed
            if (
                Host == null ||
                Host?.State != EnumEntityState.Active ||
                Host?.Alive == false ||
                hostIsOnline == false ||
                (hostIsFlying == false && Passenger == null)
            ) {
                Passenger?.TryUnmount();
                Die(EnumDespawnReason.Removed);
                return;
            }

            // Sync position to host entity
            if (Host != null)
            {
                SidedPos.SetFrom(Host.SidedPos);
            }

            // set passenger yaw
            if (Passenger != null)
            {
                Passenger.BodyYaw = Host.SidedPos.Yaw;
            }

            // play animation
            // TODO: can we make this more efficient?
            if (Api.Side == EnumAppSide.Client)
            {
                var animManager = Passenger?.AnimManager;
                if (animManager != null)
                {
                    string currentAnimation;
                    string otherAnimation;
                    if (hostIsFlying)
                    {
                        currentAnimation = AnimationWhenFlying;
                        otherAnimation = AnimationOnGround;
                    }
                    else
                    {
                        currentAnimation = AnimationOnGround;
                        otherAnimation = AnimationWhenFlying;
                    }

                    animManager.StopAnimation(otherAnimation);
                    if (!animManager.IsAnimationActive(currentAnimation))
                    {
                        animManager.StartAnimation(new AnimationMetaData()
                        {
                            Code = currentAnimation,
                            Animation = currentAnimation,
                            AnimationSpeed = 1f,
                            EaseInSpeed = 0.2f,
                            EaseOutSpeed = 0.2f,
                            Weight = 1f
                        });
                    }
                }
            }
            
            base.OnGameTick(dt);
        }

        public virtual void OnRenderFrame(float dt, EnumRenderStage stage)
        {
            // Client side we update every frame for smoother turning
            if (capi.IsGamePaused) return;

            // long ellapseMs = capi.InWorldEllapsedMilliseconds;

            // var esr = Properties.Client.Renderer as EntityShapeRenderer;
            // if (esr == null) return;

            // Sync position to host entity
            if (Passenger != null && Host != null)
            {
                // Console.WriteLine($"Mount: {EntityId} {Host.EntityId} {Passenger.EntityId} mount? = {Passenger.MountedOn != null}");
                var mountPos = MountPosition;
                Passenger.SidedPos.SetFrom(mountPos);
                // Passenger.BodyYaw = Host.SidedPos.Yaw;
            }
        }

        public virtual bool IsMountedBy(Entity entity) => Passenger == entity;

        public virtual Vec3f GetMountOffset(Entity entity)
        {
            return MountOffset;
        }

        public override void OnInteract(EntityAgent byEntity, ItemSlot itemslot, Vec3d hitPosition, EnumInteractMode mode)
        {
            return;
        }

        /// <summary>
        /// TODO: this crashes when player sneak clicks player wtf tyron
        /// </summary>
        /// <param name="action"></param>
        /// <param name="on"></param>
        /// <param name="handled"></param>
        public void OnControls(EnumEntityAction action, bool on, ref EnumHandling handled)
        {
            // unmount on client, send to server
            // because server does not recognize this action lmao
            if (Api.Side == EnumAppSide.Client)
            {
                if (Passenger != null && action == EnumEntityAction.Sneak && on)
                {
                    // Api.Logger.Notification($"TRY UNMOUNTING OnControls: {action}, {on}");
                    Passenger.TryUnmount();
                    controls.StopAllMovement();
                    Api.ModLoader.GetModSystem<PonyRaceMod>()?.ClientUnmountRequest(EntityId);
                }
            }
        }

        public override bool CanCollect(Entity byEntity) => false;

        public override void ToBytes(BinaryWriter writer, bool forClient)
        {
            base.ToBytes(writer, forClient);
            writer.Write(Host?.EntityId ?? 0);
            writer.Write(Passenger?.EntityId ?? 0);
        }

        public override void FromBytes(BinaryReader reader, bool fromServer)
        {
            base.FromBytes(reader, fromServer);

            long hostId = reader.ReadInt64();
            HostEntityIdForInit = hostId;

            long entityId = reader.ReadInt64();
            PassengerEntityIdForInit = entityId;
        }

        public virtual bool IsEmpty()
        {
            return Passenger == null;
        }

        public override WorldInteraction[] GetInteractionHelp(IClientWorldAccessor world, EntitySelection es, IClientPlayer player)
        {
            return base.GetInteractionHelp(world, es, player);
        }

        readonly Vec3f ZeroMountOffset = new Vec3f(0, 0, 0);

        public Vec3f MountOffset
        {
            get
            {
                if (Host == null) return ZeroMountOffset;

                AttachmentPointAndPose apap = Host.AnimManager.Animator?.GetAttachmentPointPose(MountAttachPoint);
                if (apap != null && apap.AttachPoint != null)
                {
                    float rotX = Host.Properties.Client.Shape != null ? Host.Properties.Client.Shape.rotateX : 0;
                    float rotY = Host.Properties.Client.Shape != null ? Host.Properties.Client.Shape.rotateY : 0;
                    float rotZ = Host.Properties.Client.Shape != null ? Host.Properties.Client.Shape.rotateZ : 0;
                    float bodyYaw = (Host is EntityAgent) ? (Host as EntityAgent).BodyYaw : 0;
                    float bodyPitch = (Host is EntityPlayer) ? (Host as EntityPlayer).WalkPitch : 0;
                    
                    // Console.WriteLine($"[{entity}] rotX: {rotX}, rotY: {rotY}, rotZ: {rotZ}, bodyYaw: {bodyYaw}, bodyPitch: {bodyPitch} entity.SidedPos.Yaw: {entity.SidedPos.Yaw} entity.SidedPos.Roll: {entity.SidedPos.Roll}");
                    // bodyYaw = (entity is EntityPlayer) ? (bodyYaw - (float)Math.PI/2) : bodyYaw; // FUCK THIS ENGINE SO FUCKING MUCH

                    // final yaw offset
                    float yawOffset = (Host is EntityPlayer) ? bodyYaw : Host.SidedPos.Yaw;
                    yawOffset += (float) Math.PI/2; // retarded

                    var mat = new Matrixf()
                        .RotateX(Host.SidedPos.Roll + rotX * GameMath.DEG2RAD)
                        .RotateY(yawOffset + rotY * GameMath.DEG2RAD)
                        .RotateZ(bodyPitch + rotZ * GameMath.DEG2RAD)
                        .Scale(Host.Properties.Client.Size, Host.Properties.Client.Size, Host.Properties.Client.Size)
                        .Translate(-0.5f, 0, -0.5f)
                        .Mul(apap.AnimModelMatrix)
                        // .Translate(ap.PosX / 16f, ap.PosY / 16f, ap.PosZ / 16f) // not needed anymore? 1.19
                    ;

                    float[] pos = new float[4] { 0, 0, 0, 1 };
                    float[] endVec = Mat4f.MulWithVec4(mat.Values, pos);

                    float dx = endVec[0];
                    float dy = endVec[1];
                    float dz = endVec[2];

                    // AttachmentPoint ap = apap.AttachPoint;
                    // Console.WriteLine($"apap: {ap.Code} {apap}, {apap.AnimModelMatrix} {mat}");
                    // Console.WriteLine($"ap: {ap.Code}, x,y,z = {ap.PosX}, {ap.PosY}, {ap.PosZ}");
                    // Console.WriteLine($"ap: {ap.Code} dx,dy,dz = {dx}, {dy}, {dz}");

                    return new Vec3f(dx, dy, dz);
                }
                else
                {
                    return ZeroMountOffset;
                }
            }
        }

        EntityPos mountPos = new EntityPos();
        
        public EntityPos MountPosition
        {
            get
            {
                var pos = Host.SidedPos;
                var moffset = MountOffset;

                mountPos.SetPos(
                    pos.X + moffset.X,
                    pos.Y + moffset.Y,
                    pos.Z + moffset.Z
                );

                mountPos.SetAngles(
                    pos.Roll,
                    pos.Yaw,
                    pos.Pitch
                );

                return mountPos;
            }
        }

        public void DidMount(EntityAgent entityAgent)
        {
            // Api.Logger.Notification($"{entityAgent.Api.Side} DIDMOUNT {entityAgent}");

            if (Passenger != null && Passenger != entityAgent)
            {
                Passenger.TryUnmount();
                return;
            }

            Passenger = entityAgent;
        }

        public override void OnEntityDespawn(EntityDespawnData despawn)
        {
            base.OnEntityDespawn(despawn);

            // Api.Logger.Notification($"DESPAWNING {EntityId} {despawn.Reason}");

            // remove host flight link
            if (HostPonyFlight != null)
            {
                HostPonyFlight.MountEntity = null;
            }

            // cleanup passenger
            RemovePassenger();
        }

        public void DidUnmount(EntityAgent entityAgent)
        {
            // Api.Logger.Notification($"UNMOUNTING {entityAgent} from {Host}");

            if (Passenger == null) return;

            RemovePassenger();

            // you should kill yourself NOW
            Die(EnumDespawnReason.Removed);
        }

        public void RemovePassenger()
        {
            EntityAgent passenger = Passenger;
            if (passenger == null) return;
            Passenger = null;

            // note this re-calls DidUnmount, need Passenger == null
            // check to avoid this recursing infinitely
            passenger.TryUnmount();

            passenger.Pos.Roll = 0;

            passenger.AnimManager?.StopAnimation(AnimationWhenFlying);
            passenger.AnimManager?.StopAnimation(AnimationOnGround);
        }

        public void MountableToTreeAttributes(TreeAttribute tree)
        {
            tree.SetString("className", "ponymount");
            tree.SetLong("entityId", EntityId);
        }
        
        public static IMountable GetMountable(IWorldAccessor world, TreeAttribute tree)
        {
            return world.GetEntityById(tree.GetLong("entityId")) as EntityPonyMount;
        }

        public void Dispose()
        {
            
        }
    }
}
