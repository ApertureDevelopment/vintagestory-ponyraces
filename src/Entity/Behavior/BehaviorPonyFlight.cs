using System;
using kemono;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace ponyraces
{
    // TODO: modify max stamina from hunger saturation
    // https://github.com/anegostudios/vsessentialsmod/blob/f102718b2ae790e9290340d1bca008535588d7c4/Entity/Behavior/BehaviorHunger.cs#L411
    public class EntityBehaviorPonyFlight : EntityBehavior
    {
        // max game light brightness
        const float MAX_BRIGHTNESS = 32;

        // flight animation
        public static string AnimationFly = "creativefly";

        // mod instance
        private readonly PonyRaceMod mod;

        // stamina tree attribute
        ITreeAttribute attribute;

        // list of traits that allow flight
        public static string[] FlightTraits = {"flight", "flightweak", "flightdark"};
        public static string[] FlightDarkTraits = {"flightdark"};

        // flag whether entity can fly
        public bool CanFly = false;

        // flag that entity must fly in darkness, otherwise higher stamina loss
        // TODO: make these plugin config options
        public bool FlyInDark = false;

        private float maxStaminaCached = -1f;
        private float currentStaminaCached = -1f;
        private float regenRateCached = -1f;

        // entity for player mounting
        // if not null, a passenger is mounted on this
        // use this to check for applying mount hunger/stamina loss modifiers
        internal EntityPonyMount MountEntity = null;

        public float CurrentStamina
        {
            get { return currentStaminaCached = attribute.GetFloat("currentStamina"); }
            set {
                if (value != currentStaminaCached)
                {
                    currentStaminaCached = value;
                    attribute.SetFloat("currentStamina", value);
                    entity.WatchedAttributes.MarkPathDirty("ponyflight");
                }
                
            }
        }

        public float MaxStamina
        {
            get { return maxStaminaCached; }
            set {
                maxStaminaCached = value;
                attribute.SetFloat("maxStamina", value);
                entity.WatchedAttributes.MarkPathDirty("ponyflight");
            }
        }

        public float RegenRate
        {
            get { return regenRateCached; }
            set {
                regenRateCached = value;
                attribute.SetFloat("regen", value);
                entity.WatchedAttributes.MarkPathDirty("ponyflight");
            }
        }
        
        public bool IsFlying
        {
            get { return entity.WatchedAttributes.GetAsBool("ponyflying", false); }
            set {
                entity.WatchedAttributes.SetBool("ponyflying", value);
                entity.WatchedAttributes.MarkPathDirty("ponyflying");
            }
        }
        
        // true if player can be mounted
        public bool Mountable
        {
            get { return entity.WatchedAttributes.GetAsBool("ponymountable", false); }
            set {
                entity.WatchedAttributes.SetBool("ponymountable", value);
                entity.WatchedAttributes.MarkPathDirty("ponymountable");
            }
        }

        public EntityBehaviorPonyFlight(Entity entity) : base(entity)
        {
            mod = entity.Api.ModLoader.GetModSystem<PonyRaceMod>();
        }

        public override void Initialize(EntityProperties properties, JsonObject typeAttributes)
        {
            base.Initialize(properties, typeAttributes);

            attribute = entity.WatchedAttributes.GetTreeAttribute("ponyflight");
            if (attribute == null)
            {
                entity.WatchedAttributes.SetAttribute("ponyflight", attribute = new TreeAttribute());
                MaxStamina = 0;
                CurrentStamina = 0;
                RegenRate = 0;
            }
            else{
                MaxStamina = attribute.GetFloat("maxStamina");
                CurrentStamina = attribute.GetFloat("currentStamina");
                RegenRate = attribute.GetFloat("regen");
            }
            
            // set initial mountable to be false
            Mountable = false;

            entity.WatchedAttributes.RegisterModifiedListener("stats", OnStatsChanged);

            OnStatsChanged(); // run initial update from attribute
        }

        // update from entity stats
        public void OnStatsChanged()
        {
            var kemono = entity.World.Api.ModLoader.GetModSystem<KemonoMod>();

            // get if entity can fly
            CanFly = kemono.HasAnyRaceTrait(entity, FlightTraits);
            FlyInDark = kemono.HasAnyRaceTrait(entity, FlightDarkTraits);

            MaxStamina = entity.Stats.GetBlended("flyStamina");
            RegenRate = entity.Stats.GetBlended("flyStaminaRegen");
            CurrentStamina = Math.Min(CurrentStamina, MaxStamina);
        }

        public override void OnEntityRevive()
        {
            CurrentStamina = MaxStamina;
        }
        
        /// <summary>
        /// Handle other player mounting this by using an EntityPonyMount
        /// as actual mount entity, which tracks this entity position.
        /// </summary>
        /// <param name="byEntity"></param>
        /// <param name="itemslot"></param>
        /// <param name="hitPosition"></param>
        /// <param name="mode"></param>
        /// <param name="handled"></param>
        public override void OnInteract(EntityAgent byEntity, ItemSlot itemslot, Vec3d hitPosition, EnumInteractMode mode, ref EnumHandling handled)
        {
            base.OnInteract(byEntity, itemslot, hitPosition, mode, ref handled);

            if (mod.api.Side == EnumAppSide.Client && byEntity == mod.capi.World.Player.Entity)
            {
                // mount entity allow check
                if (!CanFly || mode != EnumInteractMode.Interact) return;
                
                // TODO: cancel request if player already mounted

                // check if player is in mounting position
                if (Mountable)
                {
                    mod.ClientMountRequest(entity.EntityId);
                }
            }
        }

        public void ToggleFly()
        {
            bool newFlyState = !IsFlying;
            IsFlying = newFlyState;

            var player = ((entity as EntityPlayer)?.Player) as IServerPlayer;
            if (player != null)
            {
                player.WorldData.FreeMove = newFlyState;
                player.BroadcastPlayerData();
            }
        }

        public override void OnGameTick(float deltaTime)
        {
            if (entity.State == EnumEntityState.Inactive)
            {
                return;
            }

            base.OnGameTick(deltaTime);

            var player = (entity as EntityPlayer)?.Player;

            // synchronize freemove and flying state on server and client
            // needed since creative mode does not trigger ToggleFly...
            // so need something else to synchronize flying state
            if (player != null && entity.Api.Side == EnumAppSide.Server)
            {
                bool isActuallyFlying = player.WorldData.FreeMove;
                if (isActuallyFlying != IsFlying)
                {
                    IsFlying = isActuallyFlying;
                }
            }

            // do stamina on server and on client if this is client player
            bool handleStamina = entity.World.Side == EnumAppSide.Server
                || (player != null && mod.capi.World.Player == player);
            
            if (handleStamina && player != null)
            {
                var playerData = player.WorldData;
                EnumGameMode mode = playerData.CurrentGameMode;

                if (player.WorldData.FreeMove && mode != EnumGameMode.Creative && mode != EnumGameMode.Spectator)
                {
                    // initial stamina and hunger loss
                    float deltaStamina = deltaTime * mod.Config.FlyStaminaLoss;
                    float hungerLoss = deltaTime * mod.Config.FlyHungerLossRate;

                    // mounted stamina and hunger loss multiplier
                    if (MountEntity != null)
                    {
                        deltaStamina = deltaStamina * mod.Config.FlyStaminaLossMountedMultiplier;
                        hungerLoss = hungerLoss * mod.Config.FlyHungerLossMountedMultiplier;
                    }

                    // additional stamina loss for ponies that need to fly in dark
                    if (FlyInDark)
                    {
                        // using a ramp function for stamina loss multiplier:
                        //               /
                        //              /
                        //   1   ______/        --> light
                        //             x
                        //       "Dark" threshold ~14
                        //
                        // slope is the FlyOutsideDarkLossMultiplier parameter
                        
                        // is this properly synced on server and client?
                        float light = entity.Api.World.BlockAccessor.GetLightLevel(
                            (int)entity.SidedPos.X,
                            (int)entity.SidedPos.Y,
                            (int)entity.SidedPos.Z,
                            EnumLightLevelType.MaxTimeOfDayLight
                        );
                        // Console.WriteLine($"light: {light} / {MAX_BRIGHTNESS}");
                        float lightMultiplier = 1 + (mod.Config.FlyOutsideDarkLossMultiplier * Math.Max(0, light - mod.Config.FlyDarkThreshold));
                        deltaStamina = deltaStamina * lightMultiplier;
                    }

                    float newStamina = Math.Max(0, CurrentStamina - deltaStamina);
                    CurrentStamina = newStamina;

                    if (newStamina <= 0)
                    {
                        playerData.FreeMove = false;
                    }

                    // hunger loss
                    entity.GetBehavior<Vintagestory.GameContent.EntityBehaviorHunger>()?.ConsumeSaturation(hungerLoss);
                }
                else // stamina regen
                {
                    float regen = deltaTime * RegenRate;
                    float newStamina = Math.Min(MaxStamina, CurrentStamina + regen);
                    CurrentStamina = newStamina;
                }
            }

            // animations
            var animManager = entity.AnimManager;
            if (animManager != null)
            {
                if (IsFlying)
                {
                    animManager.StopAnimation("idle");
                    animManager.StopAnimation("walk");
                    animManager.StopAnimation("sprint");
                    animManager.StartAnimation(AnimationFly);
                }
                else
                {
                    animManager.StartAnimation("idle");
                    animManager.StopAnimation(AnimationFly);
                }
            }
        }

        public override string PropertyName()
        {
            return "ponyflight";
        }
    }
}
